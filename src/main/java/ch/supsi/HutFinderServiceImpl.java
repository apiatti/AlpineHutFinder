package ch.supsi;

import javax.ejb.Stateless;
import javax.jws.WebService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.Normalizer;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Stateless
@WebService(portName = "HutFinderPort", serviceName = "HutFinderService", targetNamespace = "http://www.fat-ti.ch",
        endpointInterface = "ch.supsi.HutFinderService")

public class HutFinderServiceImpl implements HutFinderService {

    /**
     * Generate a  GET request to resources host
     *
     * @return the html page ready to be parsed
     */
    public String GETRequest() {
        URL url;
        InputStream is = null;
        BufferedReader br;
        String line;
        StringBuilder sb = new StringBuilder();

        try {
            url = new URL("http://www.fat-ti.ch/capanne/");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "text/html; charster=UTF-8");
            br = new BufferedReader(new InputStreamReader(con.getInputStream()));

            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException ioe) {
            }
        }
        return sb.toString();
    }

    /**
     * Normalize text, replace à->a, ecc... for improved search results
     *
     * @param text
     * @return
     */
    public String normalizeText(final String text) {
        return Normalizer.normalize(text, Normalizer.Form.NFD)
                .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }

    /**
     * Pattern to match the hut infos, given hut's name
     *
     * @param hutName
     * @return
     */
    public String matchHutByNamePattern(final String hutName) {

        return "<h2>".concat(".*?").concat("\\b").concat(hutName).concat("\\b").concat
                (".*");
    }

    /**
     * Pattern to match hut infos, given the owner's name/subname/...
     *
     * @param ownerName
     * @return
     */
    public String matchHutByOwnerPattern(final String ownerName) {


        return "<h2>".concat(".*").concat("\\b").concat(ownerName).concat("\\b").concat(".*").concat("<p>");
    }


    /**
     * Load in memory htmlpage and splits it by new line '\n'
     *
     * @param htmlPage
     * @return String[] splitted HtmlPage
     */
    public String[] getSplittedHtml(final String htmlPage) {

        String[] splittedPage = htmlPage.split("\n");
        return splittedPage;
    }

    @Override
    public String getHutByName(final String hutName) {
        if (hutName.isEmpty()) {
            return null;
        }
        int lineN = 0;
        String htmlPage = normalizeText(GETRequest());
        String htmlBlock;
        Matcher m;
        String pattern = matchHutByNamePattern(normalizeText(hutName));
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        StringBuilder sb = new StringBuilder();
        Scanner scanner = new Scanner(htmlPage);

        while (scanner.hasNextLine()) {
            m = r.matcher(scanner.nextLine());
            lineN++;
            if (m.find()) {
                htmlBlock = m.group(0);
                sb.append(htmlBlock);
                sb.append("<hr class='divider'>");

                String imageUrlLine = getSplittedHtml(htmlPage)[lineN - 13];
                Pattern patt = Pattern.compile("<img src=\".*?\"  />", Pattern.CASE_INSENSITIVE);
                Matcher match = patt.matcher(imageUrlLine);

                if (match.find()) {
                    sb.append(match.group(0));

                }

            }

        }

        return sb.toString().replaceAll("<hr />", "");

    }

    @Override
    public String getHutByOwner(final String ownerName) {
        if (ownerName.isEmpty()) {
            return null;
        }
        int lineN = 0;
        String htmlPage = normalizeText(GETRequest());
        String htmlBlock;
        Matcher m;
        String pattern = matchHutByOwnerPattern(normalizeText(ownerName));
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        StringBuilder sb = new StringBuilder();


        Scanner scanner = new Scanner(htmlPage);
        while (scanner.hasNextLine()) {
            m = r.matcher(scanner.nextLine());
            lineN++;
            if (m.find()) {
                htmlBlock = m.group(0);
                sb.append(htmlBlock);
                sb.append("<hr class='divider'/>");
                String imageUrlLine = getSplittedHtml(htmlPage)[lineN - 13];
                Pattern patt = Pattern.compile("<img src=\".*?\"  />", Pattern.CASE_INSENSITIVE);
                Matcher match = patt.matcher(imageUrlLine);

                if (match.find()) {
                    sb.append(match.group(0));

                }

            }

        }

        return sb.toString().replaceAll("<hr />", "");

    }
}