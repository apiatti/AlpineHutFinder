package ch.supsi;

import java.util.List;

/**
 * Created by aris on 13.04.17.
 */
public class Contact {

    private final String name;
    private final String address;
    private final List<String> phones;
    private final List<String> emails;
    private final String trek;


    public Contact(final String name, final String address, final List<String> phones, final List<String> emails, final String trek) {

        this.name = name;
        this.address = address;
        this.phones = phones;
        this.emails = emails;
        this.trek = trek;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public List<String> getPhones() {
        return phones;
    }

    public List<String> getEmails() {
        return emails;
    }

    public String getTrek() {
        return trek;
    }
}
