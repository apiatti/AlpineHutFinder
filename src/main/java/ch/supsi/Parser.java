package ch.supsi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by aris on 13.04.17.
 */
public class Parser {


    private final String html;
    private Hut hut = new Hut();

    public Parser(final String html) {
        this.html = html;
    }

    public String getHtml() {
        return html;
    }


    public Gson createJson() {
        setHut();
        GsonBuilder builder = new GsonBuilder();

        Gson gson = builder.create();
        System.out.println(gson.toJson(hut));

        return gson;

    }

    /**
     * Populate the hut object
     */
    public void setHut() {
        extractHutName();
        extractAltitude();
        extractBeds();
        extractPhone();
        extractOwner();
        extractKeeper();
        extractMapAndCoordinates();
        extractContact();
    }

    //tested
    public void extractHutName() {

        //match everything until numbers
        String pattern = "<h2>.*?(?=[0-9])";
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

        Matcher m = r.matcher(html);
        if (m.find()) {
            hut.setName(m.group(0).replace("<h2>", ""));
            System.out.println(hut.getName());

        } else {
            try {
                throw new Exception();
            } catch (Exception e) {
                System.err.println("Not Hut Found - ERROR");
                e.printStackTrace();

            }
        }

    }

    public void extractAltitude() {

        //match the altitude
        String pattern = "[^<h2>.*?][0-9]{3,4}?\\s[a-z]";
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

        Matcher m = r.matcher(html);
        if (m.find()) {
            hut.setAltitude(m.group(0).replaceAll("^\\s", ""));
            System.out.println(hut.getAltitude());

        } else {
            hut.setAltitude(null);
        }

    }

    public void extractBeds() {
        //match number of beds
        String pattern = "Posti letto.*?[0-9]{1,2}+";
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);


        Matcher m = r.matcher(html);
        if (m.find()) {
            hut.setBeds(Integer.parseInt(m.group(0).replaceAll(".*?[^0-9]", "")));
            System.out.println(hut.getBeds());

        } else {
            try {
                throw new Exception();
            } catch (Exception e) {
                System.err.println("No Beds Found - ERROR");
                e.printStackTrace();
            }
        }
    }

    public void extractPhone() {
        //match phone number
        String pattern = "Telefono in capanna.*?[\\s0-9]{14}+";
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

        Matcher m = r.matcher(html);
        if (m.find()) {

            hut.setPhone((m.group(0).replaceAll(".*?[^\\s0-9]", "").replaceAll("^\\s+", "")));
            System.out.println(hut.getPhone());

        } else {

            hut.setPhone(null);
        }

    }

    public void extractOwner() {

        String pattern = "Proprietario.*?</p>";
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        Document doc;
        Matcher m = r.matcher(html);
        if (m.find()) {

            String cleaned = m.group(0).replaceAll("Proprietario(:)?", "").replaceAll("^\\s+", "");
            doc = Jsoup.parse(cleaned);
            hut.setOwner(doc.text());
            System.out.println(hut.getOwner());

        } else {

            hut.setOwner(null);
        }
    }

    public void extractKeeper() {

        String pattern = "Guardiano.*?Proprietario";
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

        Matcher m = r.matcher(html);
        if (m.find()) {
            String patt = "\\b(non)?(\\s?)custodita\\b";
            Pattern p = Pattern.compile(patt);
            Matcher match = p.matcher(m.group(0).toLowerCase());


            if (match.find()) {
                hut.setKeeper(match.group(0));

            } else {
                String keeperInfos = (m.group(0).replace("&nbsp;", "").replace("Guardiano  ", "").
                        replace("Guardiano:", "").replace("Proprietario", "").
                        replace("<br />", " "));

                hut.setKeeper(keeperInfos);
                System.out.println(hut.getKeeper());

            }


        } else {
            hut.setKeeper(null);
        }
    }

    public void extractMapAndCoordinates() {

        String pattern = "Cartina / coordinate(:)?.*?Guardiano";
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

        Matcher m = r.matcher(html);
        if (m.find()) {

            String cleaned = m.group(0).replaceAll("Cartina / coordinate(:)?(\\s)?", "")
                    .replaceAll
                            ("(\\s)?Guardiano", "");
            Document doc = Jsoup.parse(cleaned);

            cleaned = doc.text();

            String[] splitted = cleaned.split(" - |<br />| / | -");

            hut.setMap(splitted[0]);
            hut.setCoordinates(splitted[1]);
            System.out.println(hut.getMap());
            System.out.println(hut.getCoordinates());

        } else {
            hut.setMap(null);
            hut.setCoordinates(null);
        }
    }

    public void extractContact() {

        String pattern = "<h3>.*?<div class=\"sqs-block";
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

        Matcher m = r.matcher(html);
        if (m.find()) {
            Document doc = Jsoup.parse(m.group(0));
            //TODO to fix too much greedy
            //(?i) ignore cases
            hut.setContact(doc.text().replaceAll("(?i)informazioni", "").replaceAll("^\\s+", ""));
            System.out.println(hut.getContact());

        } else {
            hut.setContact(null);
        }
    }

}
