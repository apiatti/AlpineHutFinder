package ch.supsi.test;

import ch.supsi.Parser;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.Normalizer;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by aris on 13.04.17.
 */
public class Test {

    /**
     * Generate a  GET request to resources host
     *
     * @return the html page ready to be parsed
     */
    public static String GETRequest() {
        URL url;
        InputStream is = null;
        BufferedReader br;
        String line;
        StringBuilder sb = new StringBuilder();

        try {
            url = new URL("http://www.fat-ti.ch/capanne/");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "text/html; charster=UTF-8");
            br = new BufferedReader(new InputStreamReader(con.getInputStream()));

            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException ioe) {
            }
        }
        System.out.println(sb.toString());
        return sb.toString();
    }

    /**
     * Normalize text, replace à->a, ecc... for improved search results
     *
     * @param text
     * @return
     */
    public static String normalizeText(final String text) {
        return Normalizer.normalize(text, Normalizer.Form.NFD)
                .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }

    /**
     * Pattern to match the hut infos, given hut's name
     *
     * @param hutName
     * @return
     */
    public static String matchHutByNamePattern(final String hutName) {

        //return "<h2>".concat(hutName).concat(".*").concat("</h2>").concat(".*");
        return "<h2>".concat(".*?").concat("\\b").concat(hutName).concat("\\b").concat
                (".*");
    }


    /**
     * Pattern to match hut infos, given the owner's name/subname/...
     *
     * @param ownerName
     * @return
     */
    public static String matchHutByOwnerPattern(final String ownerName) {


        return "<h2>".concat(".*").concat("\\b").concat(ownerName).concat("\\b").concat(".*");
    }

    public static String getHubByOwner(final String ownerName) {
        if (ownerName.isEmpty()) {
            return null;
        }
        int lineN = 0;
        String htmlPage = normalizeText(GETRequest());
        String htmlBlock;
        Matcher m;
        String pattern = matchHutByOwnerPattern(normalizeText(ownerName));
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        StringBuilder sb = new StringBuilder();


        Scanner scanner = new Scanner(htmlPage);
        while (scanner.hasNextLine()) {
            m = r.matcher(scanner.nextLine());
            lineN++;
            if (m.find()) {
                htmlBlock = m.group(0);
                sb.append(htmlBlock);
                sb.append("<hr class='divider'/>");
                String imageUrlLine = getSplittedHtml(htmlPage)[lineN - 13];
                Pattern patt = Pattern.compile("<img src=\".*?\"  />", Pattern.CASE_INSENSITIVE);
                Matcher match = patt.matcher(imageUrlLine);

                if (match.find()) {
                    sb.append(match.group(0));

                }

            }

        }

        return sb.toString().replaceAll("<hr />", "").replaceAll("<p>I dati, la cartina e le foto per la realizzazione di questa pagina sono stati ripresi dal libro: “Capanne e rifugi del Ticino e della Mesolcina” (© Salvioni Edizioni, Bellinzona).</p><p>Informazioni sulle capanne: <a target=\"_blank\" href=\"http://www.capanneti.ch\">www.capanneti.ch</a></p><p> </p></div></div><div class=\"sqs-block button-block sqs-block-button\" data-block-type=\"53\" id=\"block-yui_3_17_2_3_1457083510576_78392\"><div class=\"sqs-block-content\"><hr class='divider'/>", "");


    }

    public static String[] getSplittedHtml(final String htmlPage) {

        String[] splittedPage = htmlPage.split("\n");
        return splittedPage;
    }


    public static String getHuByName(final String hutName) {
        if (hutName.isEmpty()) {
            return null;
        }
        int lineN = 0;
        String htmlPage = normalizeText(GETRequest());
        String htmlBlock;
        Matcher m;
        String pattern = matchHutByNamePattern(normalizeText(hutName));
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        StringBuilder sb = new StringBuilder();
        Scanner scanner = new Scanner(htmlPage);

        while (scanner.hasNextLine()) {
            m = r.matcher(scanner.nextLine());
            lineN++;
            if (m.find()) {
                htmlBlock = m.group(0);
                sb.append(htmlBlock);
                sb.append("<hr class='divider'>");

                String imageUrlLine = getSplittedHtml(htmlPage)[lineN - 13];
                Pattern patt = Pattern.compile("<img src=\".*?\"  />", Pattern.CASE_INSENSITIVE);
                Matcher match = patt.matcher(imageUrlLine);

                if (match.find()) {
                    sb.append(match.group(0));

                }

            }

        }

        return sb.toString().replaceAll("<hr />", "");
    }

    public static void main(String[] args) {

        System.out.println(getHuByName("Al Legn"));
        //getHubByOwner("UTOE Bellinzona");

    }
}

