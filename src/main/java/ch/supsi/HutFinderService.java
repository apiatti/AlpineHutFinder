package ch.supsi;

// WS Interface, ASO Course Example LS Feb 2017

import javax.jws.WebService;

@WebService(targetNamespace = "http://www.fat-ti.ch/capanne/")
public interface HutFinderService {


    String getHutByName(final String hutName);

    String getHutByOwner(final String owner);

}