package ch.supsi;

/**
 * Created by aris on 13.04.17.
 */
public class Hut {
    private String name;
    private String altitude;
    private int beds;
    private String phone;
    private String keeper;
    private String owner;
    private String contact;
    private String map;
    private String coordinates;



    public Hut(final String name, final String altitude, final int beds, final String map, final String coordinates,
               final String phone, final String keeper, final String owner, final String contact) {

        this.name = name;
        this.altitude = altitude;
        this.beds = beds;
        this.map = map;
        this.coordinates = coordinates;
        this.phone = phone;
        this.keeper = keeper;
        this.owner = owner;
        this.contact = contact;
    }

    public Hut() {

    }

    public String getName() {
        return name;
    }

    public String getAltitude() {
        return altitude;
    }

    public int getBeds() {
        return beds;
    }

    public String getPhone() {
        return phone;
    }

    public String getKeeper() {
        return keeper;
    }

    public String getOwner() {
        return owner;
    }

    public String getContact() {
        return contact;
    }

    public String getMap() {
        return map;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    public void setBeds(int beds) {
        this.beds = beds;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setKeeper(String keeper) {
        this.keeper = keeper;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

}
