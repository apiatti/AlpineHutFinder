/**
 * Created by aris on 18.04.17.
 */
$(document).ready(function () {

    var lastTimeCliked = new Date().getSeconds();

    function displayHuts(data) {
        var htmlText = $(data).find("return").text();
        var noImage = htmlText.replace(/\s*<img.*?>/g, '');
        var image = $('img', htmlText);


        $('.hidden-hr:hidden').last().after($(noImage));

        $('.hut-image').last().append($(image));
    }

    function clearpage() {
        $("div.col-sm-6").empty();
        $("#hutsDiv").empty();
        $("div.col-sm-3").append("<div class='hut-image'>");
        $("div.col-sm-6").append("<hr class='hidden-hr' hidden='true'>");
    }

    $(".img-rounded").click(function () {
        requestSuggested($(this).attr("alt"));
    });

    function requestSuggested(hutName) {

        clearpage();
        var soapRequest =
            '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"\
             xmlns:tic="http://www.fat-ti.ch/capanne/">\
                <soapenv:Header/>\
            <soapenv:Body>\
            <tic:getHutByName>\
            <!--Optional:-->\
            <arg0>' + hutName + '</arg0>\
            </tic:getHutByName>\
            </soapenv:Body>\
            </soapenv:Envelope>'

        $.ajax({
            type: "POST",
            url: wsUrl,
            contentType: "text/xml",
            dataType: "xml",
            data: soapRequest,
            success: processSuccess,
            error: processError
        });
    }

    var wsUrl = "http://isin03.dti.supsi.ch:9090/PIATTI_ARIS/webservices/HutFinderServiceImpl";

    $("#getHutByName").click(function () {
        if ($("#inputGetHutByName").val() == '') {
            alert("Please insert a name");
        }
        else {
            clearpage();
            var soapRequest =
                '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"\
                 xmlns:tic="http://www.fat-ti.ch/capanne/">\
                    <soapenv:Header/>\
                <soapenv:Body>\
                <tic:getHutByName>\
                <!--Optional:-->\
                <arg0>' + ($("#inputGetHutByName").val()) + '</arg0>\
            </tic:getHutByName>\
            </soapenv:Body>\
            </soapenv:Envelope>'

            $.ajax({
                type: "POST",
                url: wsUrl,
                contentType: "text/xml",
                dataType: "xml",
                data: soapRequest,
                success: processSuccess,
                error: processError
            });

        }
    });

    $("#getHutByOwnerButton").click(function () {


        var timeNow = new Date().getTime() - lastTimeCliked;
        if (timeNow < 5000) {
            alert("Wait " + Math.round(timeNow/1000) + " seconds before making a new request");
            return;
        }

        else {
            lastTimeCliked = new Date().getTime();
            clearpage();

            var soapRequest =
                '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"\
                xmlns:tic="http://www.fat-ti.ch/capanne/">\
                <soapenv:Header/>\
                <soapenv:Body>\
                <tic:getHutByOwner>\
                <!--Optional:-->\
                <arg0>' + $(".selectpicker").find("option:selected").text() + '</arg0>\
     </tic:getHutByOwner>\
     </soapenv:Body>\
     </soapenv:Envelope>'

            $.ajax({
                type: "POST",
                url: wsUrl,
                contentType: "text/xml",
                dataType: "xml",
                data: soapRequest,
                success: processSuccess,
                error: processError

            });

        }

    });

    function processSuccess(data, status, req) {
        if (status == "success") {
            displayHuts(data);
        }
    }

    function processError(data, status, req) {
        alert("Hut Not found");
    }


});